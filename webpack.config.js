const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
//const cssExtract = new ExtractTextPlugin({ filename: 'styles.css', allChunks: true })
const devMode = true;
module.exports = {
  context: path.join(__dirname, 'src'),
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: devMode ? '[name].css' : '[name].[hash].css',
      chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
    }),
  ],
  entry: [
    './index.js',
  ],
  output: {
    path: __dirname,
    filename: 'bundle.js',
    publicPath: '/www/'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              localIdentName: '[name]__[local]__[hash:base64:5]'
            }
          },
          devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
          'postcss-loader'
        ]
      },
      {
        test: /\.scss$/,
        use: [
          devMode ? 'style-loader' : MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: true,
              sourceMap: true,
              importLoaders: 2,
              localIdentName: '[name]__[local]__[hash:base64:5]'
            }
          },
          {
            loader: 'sass-loader',
            options: {
              includePaths: [path.resolve(__dirname, './src/styles')]
            }
          }
        ]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        include: __dirname,
      }, {
        test: /\.(jpe?g|gif|png|svg|woff|ttf|wav|mp3)$/, loader: 'file',
      }],
  },
  resolve: {
    modules: [
      path.join(__dirname, 'node_modules'),
    ],
  },
};