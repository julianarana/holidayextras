import { useState, useEffect } from 'react';
import jsonp from 'jsonp';
import { API_KEY } from '../config';
const BASE_URL = `https://api.flickr.com/services/rest`;

function encodeObjectToUri(object) {
  return Object.keys(object)
    .map((key) => `${encodeURIComponent(key)}=${encodeURIComponent(object[key])}`)
    .join('&');
}

export function usePhotos(search, nextPage) {
  const params = {
    jsoncallback: 'responseHandler',
    format: 'json',
    api_key: API_KEY,
    method: search ? 'flickr.photos.search' : 'flickr.photos.getRecent',
    extras: 'owner_name, tags, description, url_m, path_alias',
    tags: search,
    safe_search: 1,
  }
  const URL = `${BASE_URL}?${encodeObjectToUri(params)}`;
  const [photos, setPhotos] = useState({ items: [], page: -1, total_pages: 0 });

  function responseHandler(value) {
    const items = value.photos.photo.map((photo) => ({
      author: photo.ownername,
      authorLink: `https://www.flickr.com/people/${photo.owner}`,
      description: photo.description._content,
      tags: photo.tags.length > 0 ? photo.tags.split(' ') : [],
      title: photo.title,
      image: photo.url_m,
      link: photo.pathalias ? `https://www.flickr.com/photos/${photo.pathalias}/${photo.id}/` : null,
    }));

    const prevItems = photos.items.slice(0);
    const newItems = prevItems.concat(items);
    setPhotos({ items: newItems, page: value.photos.page, total_pages: value.photos.pages })
  }

  useEffect(() => {
    setPhotos({ items: [], page: -1, total_pages: 0 })
    jsonp(URL)
  }, [search]);

  useEffect(() => {
    if (nextPage > 0) {
      const pagedParams = Object.assign({ page: nextPage }, params);
      const pagedURL = `${BASE_URL}?${encodeObjectToUri(pagedParams)}`;
      jsonp(pagedURL)
    }
  }, [nextPage])


  window.responseHandler = responseHandler.bind(this);
  return photos;
}
