import React from 'react';
import FlickerResults from '../components/FlickerResults';
import styles from './mainView.scss';

export default function MainView(props) {

  return <div className={styles.view}>
    <FlickerResults />
  </div>
}