import React from 'react';
import ReactDOM from 'react-dom';
import MainView from './views/MainView';
import styles from './index.scss';


class FlickerTask extends React.Component {

  render() {
    return <div className={styles.content}>
      <MainView />
    </div>
  }
}


document.addEventListener('DOMContentLoaded', () => ReactDOM.render(
  React.createElement(FlickerTask),
  document.getElementById('app')
))