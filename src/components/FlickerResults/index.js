import React, { useState, useEffect } from 'react';
import FlickerPhoto from './FlickerPhoto';
import SearchField from './SearchField';
import styles from './flickerResults.scss';
import { usePhotos } from '../../api/requests';

export default function FlickerResults() {

  const [searchValue, setSearchValue] = useState('');
  const [page, setPage] = useState(0);
  const results = usePhotos(searchValue, page);

  function handleScrolling(event) {
    const element = event.target
    if (element.scrollHeight - element.scrollTop === element.clientHeight) {
      if (results.page < results.total_pages) {
        setPage(results.page + 1);
      }
    }
  }

  function handleSearchChange(value) {
    setSearchValue(value);
  }

  function getResults() {
    return <div onScroll={handleScrolling}>
      <header className={styles.header}>
        <h1 className={styles['header--title']}>Flicker Photo Stream</h1>
        <div className={styles['header--search']}>
          <SearchField doSearch={handleSearchChange} placeholder="Search by tag" />
        </div>
      </header>
      <section className={styles.photos}>
        {results.items && results.items.map((item, index) => <FlickerPhoto key={index} {...item} />)}
        {results.items && !results.items.length && <div className={styles.empty}>No Results found...</div>}
      </section>
    </div>
  }

  return <div>{getResults()}</div>
}