import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './searchField.scss';

let timeout;
function SearchField({ doSearch, placeholder }) {
    const [value, setValue] = useState('');

    function handleChange(event) {
        const editedValue = event.target.value;
        if (timeout) {
            clearTimeout(timeout);
        }
        timeout = setTimeout(() => doSearch(editedValue), 400);
        setValue(editedValue);
    }

    return <div>
        <input type='text' value={value} onChange={handleChange}
            className={styles.input}
            placeholder={placeholder} />
    </div>
}

SearchField.propTypes = {
    doSearch: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
}

export default SearchField;