import React from 'react';
import PropTypes from 'prop-types';
import styles from './flickerPhoto.scss';

function FlickerPhoto({ author, authorLink, description, link, image, title, tags }) {
  return <div className={styles.photo}>
    <div className={styles['photo-image']}>
      <img src={image} alt={title} />
    </div>
    <div className={styles['photo-credits']}>
      {link && <a href={link} target="_blank">{title}</a>}
      {!link && title}
      &nbsp;by <a href={authorLink} target="_blank">{author}</a>
    </div>
    <div>
      {description && <div className={styles['photo-description']}>
        <h5 className={styles['photo-description--title']}>Description</h5>
        <div className={styles['photo-description--content']} dangerouslySetInnerHTML={{ __html: description }} ></div>
      </div>}
      {tags && tags.length > 0 && <div className={styles['photo-tags']}>
        <h5 className={styles['photo-tags--title']}>Tags: </h5>
        <span className={styles['photo-tags--content']}>{tags.join(', ')}</span>
      </div>}
    </div>
  </div >
}

FlickerPhoto.propTypes = {
  author: PropTypes.string,
  authorLink: PropTypes.string,
  description: PropTypes.string,
  image: PropTypes.string,
  link: PropTypes.string,
  title: PropTypes.string,
  tags: PropTypes.arrayOf(PropTypes.string)
}

export default FlickerPhoto;